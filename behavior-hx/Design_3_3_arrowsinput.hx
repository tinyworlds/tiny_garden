package scripts;

import com.stencyl.graphics.G;
import com.stencyl.graphics.BitmapWrapper;

import com.stencyl.behavior.Script;
import com.stencyl.behavior.Script.*;
import com.stencyl.behavior.ActorScript;
import com.stencyl.behavior.SceneScript;
import com.stencyl.behavior.TimedTask;

import com.stencyl.models.Actor;
import com.stencyl.models.GameModel;
import com.stencyl.models.actor.Animation;
import com.stencyl.models.actor.ActorType;
import com.stencyl.models.actor.Collision;
import com.stencyl.models.actor.Group;
import com.stencyl.models.Scene;
import com.stencyl.models.Sound;
import com.stencyl.models.Region;
import com.stencyl.models.Font;

import com.stencyl.Engine;
import com.stencyl.Input;
import com.stencyl.Key;
import com.stencyl.utils.Utils;

import openfl.ui.Mouse;
import openfl.display.Graphics;
import openfl.display.BlendMode;
import openfl.display.BitmapData;
import openfl.display.Bitmap;
import openfl.events.Event;
import openfl.events.KeyboardEvent;
import openfl.events.TouchEvent;
import openfl.net.URLLoader;

import box2D.common.math.B2Vec2;
import box2D.dynamics.B2Body;
import box2D.dynamics.B2Fixture;
import box2D.dynamics.joints.B2Joint;
import box2D.collision.shapes.B2Shape;

import motion.Actuate;
import motion.easing.Back;
import motion.easing.Cubic;
import motion.easing.Elastic;
import motion.easing.Expo;
import motion.easing.Linear;
import motion.easing.Quad;
import motion.easing.Quart;
import motion.easing.Quint;
import motion.easing.Sine;

import com.stencyl.graphics.shaders.BasicShader;
import com.stencyl.graphics.shaders.GrayscaleShader;
import com.stencyl.graphics.shaders.SepiaShader;
import com.stencyl.graphics.shaders.InvertShader;
import com.stencyl.graphics.shaders.GrainShader;
import com.stencyl.graphics.shaders.ExternalShader;
import com.stencyl.graphics.shaders.InlineShader;
import com.stencyl.graphics.shaders.BlurShader;
import com.stencyl.graphics.shaders.SharpenShader;
import com.stencyl.graphics.shaders.ScanlineShader;
import com.stencyl.graphics.shaders.CSBShader;
import com.stencyl.graphics.shaders.HueShader;
import com.stencyl.graphics.shaders.TintShader;
import com.stencyl.graphics.shaders.BloomShader;



class Design_3_3_arrowsinput extends SceneScript
{
	public var _field1:Actor;
	public var _field2:Actor;
	public var _field3:Actor;
	public var _field4:Actor;
	public var _field5:Actor;
	
	
	public function new(dummy:Int, dummy2:Engine)
	{
		super();
		nameMap.set("field1", "_field1");
		nameMap.set("field2", "_field2");
		nameMap.set("field3", "_field3");
		nameMap.set("field4", "_field4");
		nameMap.set("field5", "_field5");
		
	}
	
	override public function init()
	{
		
		/* =========================== Keyboard =========================== */
		addKeyStateListener("left", function(pressed:Bool, released:Bool, list:Array<Dynamic>):Void
		{
			if(wrapper.enabled && pressed)
			{
				if((Engine.engine.getGameAttribute("field_selected") < 2))
				{
					Engine.engine.setGameAttribute("field_selected", 5);
				}
				else
				{
					Engine.engine.setGameAttribute("field_selected", (Engine.engine.getGameAttribute("field_selected") - 1));
				}
				if((Engine.engine.getGameAttribute("field_selected") == 1))
				{
					_field1.setAnimation("" + "active");
					_field2.setAnimation("" + "inactive");
				}
				if((Engine.engine.getGameAttribute("field_selected") == 2))
				{
					_field2.setAnimation("" + "active");
					_field3.setAnimation("" + "inactive");
				}
				if((Engine.engine.getGameAttribute("field_selected") == 3))
				{
					_field3.setAnimation("" + "active");
					_field4.setAnimation("" + "inactive");
				}
				if((Engine.engine.getGameAttribute("field_selected") == 4))
				{
					_field4.setAnimation("" + "active");
					_field5.setAnimation("" + "inactive");
				}
				if((Engine.engine.getGameAttribute("field_selected") == 5))
				{
					_field5.setAnimation("" + "active");
					_field1.setAnimation("" + "inactive");
				}
			}
		});
		
		/* =========================== Keyboard =========================== */
		addKeyStateListener("right", function(pressed:Bool, released:Bool, list:Array<Dynamic>):Void
		{
			if(wrapper.enabled && pressed)
			{
				if((Engine.engine.getGameAttribute("field_selected") > 4))
				{
					Engine.engine.setGameAttribute("field_selected", 1);
				}
				else
				{
					Engine.engine.setGameAttribute("field_selected", (Engine.engine.getGameAttribute("field_selected") + 1));
				}
				if((Engine.engine.getGameAttribute("field_selected") == 1))
				{
					_field1.setAnimation("" + "active");
					_field5.setAnimation("" + "inactive");
				}
				if((Engine.engine.getGameAttribute("field_selected") == 2))
				{
					_field2.setAnimation("" + "active");
					_field1.setAnimation("" + "inactive");
				}
				if((Engine.engine.getGameAttribute("field_selected") == 3))
				{
					_field3.setAnimation("" + "active");
					_field2.setAnimation("" + "inactive");
				}
				if((Engine.engine.getGameAttribute("field_selected") == 4))
				{
					_field4.setAnimation("" + "active");
					_field3.setAnimation("" + "inactive");
				}
				if((Engine.engine.getGameAttribute("field_selected") == 5))
				{
					_field5.setAnimation("" + "active");
					_field4.setAnimation("" + "inactive");
				}
			}
		});
		
	}
	
	override public function forwardMessage(msg:String)
	{
		
	}
}